import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import {useDispatch, useSelector} from 'react-redux';


import * as newsAction from '../redux/actions/newsAction';

function Card(props) {


  const dispatch = useDispatch();
  const isFav = useSelector(state => state.news.favorites.some(article => article.url === props.url));


/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
  function handleClick() {
    props.navigation.navigate('NewsDetails',
    {
      articleUrl: props.url
    })
  }

  function handleFav() { 
      dispatch(newsAction.toggleFavorite(props.url))
   }

  return (
    <TouchableOpacity onPress={handleClick}>
      <View style={styles.card}>
        <View style={styles.imageWrapper}>
          <Image
            // source={require('../../assets/news-demo.jpg')}
            source={{
              uri: props.image ? props.image : 'https://res.cloudinary.com/practicaldev/image/fetch/s--3GWZPuoM--/c_imagga_scale,f_auto,fl_progressive,h_420,q_auto,w_1000/https://thepracticaldev.s3.amazonaws.com/i/fk0849hvg2rt13bpqhjy.jpg',
            }}
            style={styles.image}
          />
        </View>
        <View style={styles.titleWrapper}>
          <Text style={styles.title}>
          {props.title && props.title.length > 30 ? props.title.slice(0, 30) + '...' : props.title}
          </Text>
          <MaterialIcons 
              name={isFav ? 'favorite' : 'favorite-border'} 
              color="#72bcd4" size={24}
              onPress={handleFav}
               />
        </View>
        <View style={styles.descriptionWrapper}>
          <Text style={styles.description}>
          {props.description && props.description.length > 200  ? props.title.slice(0,200) + '...' : props.title}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: "#ffffff",
    height: 380,
    margin: 20,
    borderRadius: 10,
    shadowColor: "black",
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 5,
    elevation: 5,
  },
  imageWrapper: {
    width: "100%",
    height: "60%",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: "hidden",
  },
  image: {
    height: "100%",
    width: "100%",
  },
  titleWrapper: {
    height: "10%",
    paddingHorizontal: 15,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 10,
  },
  title: {
    fontFamily: "Ubuntu-Bold",
    fontSize: 20,
  },
  description: {
    fontFamily: "Ubuntu",
    fontSize: 15,
    marginTop: 10,
  },
  descriptionWrapper: {
    paddingHorizontal: 15,
  },
});

export default Card;
