
export const FETCH_ARTICLES = 'FETCH_ARTICLES';
export const TOGGLE_FAVORITES = 'TOGGLE_FAVORITES';


export const fetchArticles = () => {
    return async dispatch => {

      const result =  await fetch('http://newsapi.org/v2/everything?q=apple&from=2021-01-29&to=2021-01-29&sortBy=popularity&apiKey=ae40c4c2ec854b94a0a4303055b96274')

      const resultData = await result.json();
        dispatch ({    
            type: FETCH_ARTICLES,
            payload: resultData
        }); 
    }
}


export const toggleFavorite = url => {
    return {
        type: TOGGLE_FAVORITES,
        payload: url
    }
}